package com.example.vinit.sampleapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.flexmoney.FlexMoneyClient;
import com.example.flexmoney.Interface.FlexMoneyInitCallback;
import com.example.flexmoney.Interface.FlexMoneyTransactionCallback;

public class MainActivity extends AppCompatActivity {

    Button mTransaction;
    FlexMoneyClient flexMoneyClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTransaction = (Button) findViewById(R.id.mTransaction) ;

        //hide flex-money payment option initially but have enable it for testing purpose
        //mTransaction.setVisibility(View.GONE);

        mTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startTransaction();
            }
        });

        flexMoneyClient = FlexMoneyClient.getInstance(getApplicationContext());

        //initializing FlexMoney Service with dummy data
        flexMoneyClient.init(getResources().getString(R.string.access_key), "9773629405", "vinit.thaker@hotmail.com", new FlexMoneyInitCallback() {
            @Override
            public void onInitSuccessWithValidClientAndUser() {
                //Flex Service got initialized successfully and now you can show FlexPayment in your app
                //see if service started as expected by calling getServiceStatus() and enable flex-money option accordingly
                updateUI();
            }

            @Override
            public void onInitSuccessWithValidClient() {
                //if  merchant is valid but user is not pre-approved then this method gets called
            }

            @Override
            public void onFailureWithInvalidClient() {
                //if merchant is not valid then this method gets called

            }

            @Override
            public void onNetworkError() {
                //If network is not available, then this method gets called.

            }
        });
    }

    void updateUI(){
        if(flexMoneyClient.getServiceStatus()){
            mTransaction.setVisibility(View.VISIBLE);
        }
    }

    void startTransaction(){

        //check service status before starting the transaction
        flexMoneyClient.startTransaction(new FlexMoneyTransactionCallback() {
            @Override
            public void onTransactionSuccess(String message) {
                //this method get called on successful transaction
                showMessage(message);
                //show appropriate message to user
            }

            @Override
            public void onTransactionFailure(String message) {
                showMessage(message);

            }

            @Override
            public void onNetworkError() {
                //If network is not available, then this method gets called.
            }
        });
    }

    void showMessage(String message){
        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
    }
}
