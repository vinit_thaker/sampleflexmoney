package com.example.flexmoney.Model;

/**
 * Created by vinit on 7/24/2016.
 */
public class JsonResponse {

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
