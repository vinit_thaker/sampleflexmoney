package com.example.flexmoney.Model;

/**
 * Created by vinit on 7/25/2016.
 */

 /* response structure
    {
    "success": true,
    "error": null,
    "payload": {
                "data": null,
                "message": "Thanks for checking in."
                }
    }
     */

public class TransactionResponse {

    boolean success;
    String error;
    Payload payload;

    public boolean isSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }

    public Payload getPayload() {
        return payload;
    }
}


