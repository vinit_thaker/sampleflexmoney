package com.example.flexmoney.Interface;

/**
 * Created by vinit on 7/23/2016.
 */
public interface FlexMoneyTransactionCallback {

    void onTransactionSuccess(String message);

    void onTransactionFailure(String message);

    void onNetworkError();
}
