package com.example.flexmoney.Interface;

/**
 * Created by vinit on 7/25/2016.
 */
public interface FlexMoneyInitCallback {

    void onInitSuccessWithValidClientAndUser();

    void onInitSuccessWithValidClient();

    void onFailureWithInvalidClient();

    void onNetworkError();

}
