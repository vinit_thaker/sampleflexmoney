package com.example.flexmoney;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.example.flexmoney.Environment.Constant;
import com.example.flexmoney.Interface.FlexMoneyInitCallback;
import com.example.flexmoney.Interface.FlexMoneyTransactionCallback;
import com.example.flexmoney.Model.JsonResponse;
import com.example.flexmoney.Model.TransactionResponse;
import com.example.flexmoney.NetworkHelper.JsonObjectRequest;
import com.example.flexmoney.NetworkHelper.VolleyQueue;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vinit on 7/23/2016.
 */
public class FlexMoneyClient extends Application {

    private static final String TAG = "FlexMoneyClient";
    //to check whether FlexService initialized or not..
    private boolean initialized;

    //to store application context
    private Context mContext;

    //to check whether user is pre approved or not
    private boolean preApproved;

    private static FlexMoneyClient instance;



    private FlexMoneyClient(Context context) {
        initialized = false;
        this.mContext = context;
        preApproved = false;
    }

    public static FlexMoneyClient getInstance(Context context) {
        if(instance == null) {
            instance = new FlexMoneyClient(context);
        }
        return instance;
    }

    public void init(@NonNull String accessKey, String phoneNumber, String emailId, final FlexMoneyInitCallback callback) {
        if(!initialized) {

            JSONObject body = new JSONObject();
            try {
                body.put("access_key", accessKey);
                if(phoneNumber!=null)
                body.put("phone_number", phoneNumber);
                body.put("email_id", emailId);
            } catch (JSONException e) {
                Log.i(TAG, e.toString());
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, Constant.TRANSACTION_URL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();

                    JsonResponse initResponse = gson.fromJson(response.toString(), JsonResponse.class);
                    if(initResponse.getStatus()==Constant.VALID_CLIENT_PRE_APPROVED_CODE) {
                        initialized = true;
                        callback.onInitSuccessWithValidClientAndUser();
                    }else if(initResponse.getStatus() ==Constant.VALID_CLIENT_CODE){
                        callback.onInitSuccessWithValidClient();
                    }else{
                        callback.onFailureWithInvalidClient();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        Log.e(TAG, error.toString());
                        if(error instanceof NoConnectionError) {
                            callback.onNetworkError();
                        }else if(error instanceof TimeoutError) {
                            callback.onNetworkError();
                        }else{
                            callback.onFailureWithInvalidClient();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            })

            {
                @Override
                public Map getHeaders() throws AuthFailureError {
                    Map headers = new HashMap();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };

            VolleyQueue.getInstance(mContext).addToRequestQueue(jsonObjectRequest);

        }

    }

    public boolean getServiceStatus(){
        return (initialized && preApproved);
    }

    public void startTransaction(final FlexMoneyTransactionCallback callback){
        //handle various
        //open url which will return some data and send it back to

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, Constant.TRANSACTION_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();

                TransactionResponse transactionResponse = gson.fromJson(response.toString(), TransactionResponse.class);

                if(transactionResponse.isSuccess()) {
                    callback.onTransactionSuccess(transactionResponse.getPayload().getMessage());
                }else{
                    callback.onTransactionFailure(transactionResponse.getError());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                try {
                    Log.e(TAG, error.toString());

                    if(getApplicationContext()!=null&&error instanceof NoConnectionError) {
                        callback.onNetworkError();
                    }else if(getApplicationContext()!=null&&error instanceof ServerError) {
                    }else if(getApplicationContext()!=null&&error instanceof TimeoutError) {
                        callback.onNetworkError();
                    }else{
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        })

        {
            @Override
            public Map getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Token " + "OTc3MzYyOTQwNTpkZWxpdmVyeWd1eQ==");
                return headers;
            }
        };

        VolleyQueue.getInstance(mContext).addToRequestQueue(jsonObjectRequest);
    }




}
