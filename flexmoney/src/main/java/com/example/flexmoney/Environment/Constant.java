package com.example.flexmoney.Environment;

/**
 * Created by vinit on 7/23/2016.
 */
public class Constant {

    public static String INIT = "https://test.flexmoney.in/initService";
    public static String TRANSACTION_URL = "http://yourguy.herokuapp.com/api/v3/deliveryguy/0/check_in/";


    public static int VALID_CLIENT_PRE_APPROVED_CODE = 1;
    public static int VALID_CLIENT_CODE = 2;

}
